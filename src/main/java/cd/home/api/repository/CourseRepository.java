package cd.home.api.repository;

import cd.home.api.entitiy.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseRepository extends CrudRepository<Course,String> {
    public List<Course> findByTopicId(String topicId);
    public List<Course> findByName(String name);
}
