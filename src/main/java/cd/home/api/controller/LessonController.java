package cd.home.api.controller;

import cd.home.api.entitiy.Course;
import cd.home.api.entitiy.Lesson;
import cd.home.api.entitiy.Topic;
import cd.home.api.service.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class LessonController {
    @Autowired
    private LessonService lessonService;

    @RequestMapping("/topics/{topicId}/courses/{id}/lessons")
    public List<Lesson> getAllLessons(@PathVariable String id) {
        return lessonService.getAllLessons(id);
    }

    @RequestMapping("/topics/{topicId}/courses/{CourseId}/lessons/{id}")
    public Optional<Lesson> getLesson(@PathVariable String id) {
        return lessonService.getLesson(id);
    }

    @RequestMapping(value = "/topics/{topicId}/courses/{courseId}/lessons", method = RequestMethod.POST)
    public void addLesson(@RequestBody Lesson lesson, @PathVariable String courseId,@PathVariable String topicId) {
        lesson.setCourse(new Course(courseId,"","",topicId));
        lessonService.addLesson(lesson);
    }


    @RequestMapping(value = "/topics/{topicId}/courses/{courseId}/lessons/{id}", method = RequestMethod.PUT)
    public void updateLesson(@RequestBody Lesson lesson, @PathVariable String id,@PathVariable String courseId) {
        lesson.setCourse(new Course(courseId,"","",""));
        lessonService.updateLesson(lesson);
    }

    @RequestMapping(value = "/topics/{topicId}/courses/{courseId}/lessons/{id}", method = RequestMethod.DELETE)
    public void deleteLesson(@PathVariable String id) {
        lessonService.deleteLesson(id);
    }

}

    

